 <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="/img/user.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>PETUGAS,</span>
                <h2>{{Auth::guard('petugas')->user()->email}}</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-check-square-o"></i> Verifikasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/petugas/verifikasi">Verifikasi Donatur & Donasi</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-automobile"></i> Jemput <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/petugas/jemput">Jemput Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cube"></i> Distribusi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/petugas/distribusi">Distribusi Bantuan</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-users"></i> Data <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/petugas/donatur">Data Donatur</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-info-circle"></i> Informasi <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="/petugas/bencana">Informasi Bencana</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="/img/user.png" alt="">{{Auth::guard('petugas')->user()->name}}
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="{{ route('logout') }}" 
                      onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Keluar</a></li>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST">@csrf
                      </form>
                  </ul>

                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

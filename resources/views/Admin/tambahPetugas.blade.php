<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login.css">

    <title>Hello, world!</title>
  </head>
     <body>
      <!-- navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
        <a class="navbar-brand" href="#">DONATE</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="welcome.blade.php">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Tentang Kami</a>
            </li>
            <li class="nav-item">
               <a class="nav-link" href="#">Panduan Donasi</a>
            </li>
            <form class="form-inline fs">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <li class="nav-item">
                <a class="btn btn-outline-warning ds" href="/formdonasi">Donasi Sekarang</a>
            </li>
            <li class="nav-item">
              <div class="btn-group lg" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle btn-outline-primary lg" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Masuk
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <a class="dropdown-item" href="/login">Donatur</a>
                  <a class="dropdown-item" href="{{ route('petugas.login') }}">Petugas</a>
                </div>
              </div>
               <!--  <a class="btn btn-outline-primary lg" href="/login">Masuk</a> -->
            </li>
            </ul>
        </div>
        </div>
    </nav>
    <!-- akhir navbar -->
      <div class="container">
        <div class="row">
          <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
              <div class="card-body">
                <h5 class="card-title text-center">Tambah Petugas</h5>
                <form class="form-signin" method="POST" action="{{ route('tambah.petugas') }}">
                  {{ csrf_field() }}
                  <div class="form-label-group">
                    <input type="text" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Nama" required autofocus>
                    <label for="name">Nama Lengkap</label>
                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                  </div>

                   <div class="form-label-group">
                    <input type="text" id="inputAlamat" class="form-control" placeholder="Alamat" required autofocus>
                    <label for="inputAlamat">Alamat</label>
                  </div>

                  <div class="form-label-group">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                    <label for="email">Email address</label>
                     @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                  </div>

                  <div class="form-label-group">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required autofocus>
                    <label for="Password">Password</label>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    
                  </div>

                  <div class="form-label-group">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Re-Password" required>
                    <label for="password-confirm">Re-Password</label>
                  </div>

                  <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Remember password</label>
                  </div>
                  <button class="btn btn-lg btn-danger btn-block text-uppercase" type="submit">Daftar</button>
                  <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Masuk</button>
                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </body>

     <!-- footer -->
    <footer id="fo">
      <div class="container text-center">
        <div class="row">
          <div class="col-sm-12">
             <p>&copy; Copyright 2018 | Built with <i class="glyphicon glyphicon-heart"></i> by. <a href="https://www.instagram.com/ms_alif/?hl=id">
              Muhammad Sulthon Alif
            </a>.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <a href="https://www.youtube.com/Anim3Recon" class="btn btn-danger"><i class="glyphicon glyphicon-play"></i> Subscribe to Youtube</a>
          </div>
        </div>
      </div>
    </footer>
    <!-- akhir footer -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script type="text/javaScript" src="js/bootstrap.min.js" ></script>
  </body>
</html>
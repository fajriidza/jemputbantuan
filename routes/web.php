<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('utama');


Auth::routes();

//Admin
Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login')->middleware('guest');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
Route::delete('/kelolaPetugas/{hapus}', 'AdminController@destroy')->name('petugas.destroy');
Route::get('/kelolaPetugas', 'AdminController@kelolaPetugas')->name('kelolaPetugas');
Route::post('/kelolaPetugas', 'AdminController@store');
Route::patch('/kelolaPetugas/{kirim}','AdminController@update')->name('petugas.update');



//User
Route::get('/daftar', 'Auth\UserRegisterController@showRegistrationForm')->name('user.register')->middleware('guest');
Route::post('/daftar/{kirim}', 'Auth\UserRegisterController@store')->name('daftar');
Route::get('/donatur/donasi', 'DonasiController@donasiSekarang')->middleware('auth:web');
Route::get('/login', 'Auth\UserLoginController@showLoginForm')->name('user.login')->middleware('guest');
Route::post('/login', 'Auth\UserLoginController@login')->name('user.login.submit');
Route::get('/donatur', 'UserController@index')->name('dashboarduser');
Route::post('/donatur/edit/{id}', 'UserController@editProfil')->name('editProfile');
Route::get('/donatur/riwayat/{id}', 'UserController@riwayatDonasi');
Route::post('/donatur/riwayat/{id}', 'UserController@riwayatDonasi')->name('riwayatDonasi');
Route::put('/donatur/edit/{id}', 'UserController@updateProfil')->name('profil.update');
Route::get('/bencana','HomeController@daftarBencana');
Route::get('/detail/{id}','HomeController@detailbencana')->name('detail.bencana');
Route::get('/donatur/donasi/{id}','DonasiController@donasibencana')->name('donasi.bencana')->middleware('auth:web');
Route::post('/donatur/donasi', 'DonasiController@storeDonasi')->name('tambah.donasi')->middleware('auth:web');



//Petugas

Route::get('/petugas/login', 'Auth\PetugasLoginController@showLoginForm')->name('petugas.login')->middleware('guest');
Route::post('/petugas/login', 'Auth\PetugasLoginController@login')->name('petugas.login.submit');
Route::get('/petugas/verifikasi','PetugasController@verifikasi')->name('verif.donasi')->middleware('auth:petugas');;
Route::get('/petugas/verifikasi/{id}','PetugasController@editVerifikasi')->name('verif.edit')->middleware('auth:petugas');;
Route::get('/petugas/jemput', 'PetugasController@jemput')->name('jemput.donasi');
Route::get('/petugas/jemput/{id}','PetugasController@editJemput')->name('jemput.edit')->middleware('auth:petugas');;
Route::patch('/petugas/verifikasi/{id}','PetugasController@verifikasiDonasi')->name('verifikasi.donasi');
Route::get('/petugas/donatur', 'PetugasController@dataDonatur');
Route::get('/petugas/donatur/{id}', 'PetugasController@detaildataDonatur')->name('detail.donatur');
Route::get('/petugas/distribusi', 'PetugasController@distribusi')->name('distribusi.donasi');
Route::patch('/petugas/distribusi/{kirim}','PetugasController@distribusiDonasi')->name('distribusi');
Route::put('/petugas/jemput/{id}','PetugasController@atur_jadwal')->name('petugas.atur_jadwal');
Route::patch('/petugas/verifikasi{id}','PetugasController@simpanVerifikasi')->name('simpan.verifikasi');



//Petugas Bencana
Route::post('/petugas/bencana', 'PetugasController@store')->name('bencana.store');
Route::get('/petugas/bencana', 'PetugasController@bencana')->name('infobencana')->middleware('auth:petugas');
Route::get('petugas/bencana/{id}', 'PetugasController@editBencana')->name('bencana.edit');
Route::put('/petugas/bencana/{id}','PetugasController@updateBencana')->name('bencana.update');
Route::delete('/petugas/bencana/{hapus}', 'PetugasController@destroyBencana')->name('bencana.destroy');
Route::post('select-ajax', ['as'=>'select-ajax','uses'=>'LokasiController@selectAjax']);

Route::get('/maps', function () {
    return view('detailDonatur');
});
Route::get('/ya', function () {
    return view('dDonatur');
});